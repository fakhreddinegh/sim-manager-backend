# Laravel_API_sim-manager
To create a project from this repository:

`git clone  --branch=master https://fakhreddinegh@bitbucket.org/powerdevlopers/sim-manager-backend.git`
`cd sim-manager-backend`
`composer install`

#Passport Installation :
install Passport via the Composer package manager
composer require laravel/passport

#Migration :
If you are running the application for the first time you should run the migrations
`php artisan migrate`

#Passport Keys generation :
Next, you should run the passport:install command. 
This command will create the encryption keys needed to generate secure access tokens.
In addition, the command will create "personal access" and "password grant" clients 
which will be used to generate access tokens:
`php artisan passport:install`
#Fractal instalaltion
some other framework are required like laravel-fractal
`composer require spatie/laravel-fractal`

##run artisan serve 
`php artisan serve`

##Available API
### Register
url : %server_adresse%/api/register
methode : POST
body :{
  "firstName": "fakhreddine",
  "lastName": "GHEDIRA",
  "email": "fakhreddine.ghedira@esprit.tn",
  "password": "0000",
  "imei": "3783783678378",
  "imsi": "75287832783783"
}
### Authentification
url : %server_adresse%/oauth/token
methode : POST
body :{
"grant_type":"password",
"client_id":"2",
"client_secret":"8fSfiaN2j2yTdMmShwxbIyzj12Cxffe83XS8pUku",
"username":"fakhreddine.ghedira@esprit.tn",
"password":"0000",
"scope":"*"
}
response :
{
    "token_type": "Bearer",
    "expires_in": 31535999,
    "access_token": "%Token%",
    "refresh_token": "%Refresh Token"
}
### Get connected user informations
url : %server_adresse%/api/user
methode : GET
header : Authorization : Bearer %Token%
