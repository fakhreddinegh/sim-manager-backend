<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait Orderable
{
   
    public function  scopeLatestFirst($query)
    {
        return $query->orderBy('created_at','desc');
    }

    public function  scopeOldestFirst($query)
    {
        return $query->orderBy('created_at','asc');
    }
}