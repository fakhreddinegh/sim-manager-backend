<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\User;
class RegisterController extends Controller
{
     public function register(StoreUserRequest $request)
    {
        $user=new User;
        $user->id = $request->id;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password=bcrypt($request->password);
        $user->imei = $request->imei;
        $user->imsi = $request->imsi;
        $user->save();
        return $user;     
    }
}
