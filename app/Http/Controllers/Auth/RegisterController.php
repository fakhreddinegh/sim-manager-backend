<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName'=> 'string|max:255',
            'lastName'=> 'string|max:255',
            'adress'=> 'string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'string|min:6|email',
            'password'=> 'required|string|confirmed|max:255',
            'imei'=> 'string|max:255',
            'imsi'=> 'string|max:255',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
        'id'=>$data['id'],
        'firstName'=>$data['firstName'],
        'lastName'=>$data['lastName'],
        'email'=>$data['email'],
        'password'=>$data['password'],
        'imei'=>$data['imei'],
        'imsi'=>$data['imsi']
        ]);
    }
}
